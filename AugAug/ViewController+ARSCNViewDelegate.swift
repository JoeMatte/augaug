//
//  ViewController+SCNSceneRendererDelegate.swift
//  AROldAugsburg
//
//  Created by Joe Matteson on 9/18/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import Foundation
import ARKit
import SceneKit

// MARK: - Notes
/*
 When mapping the 3D model to the anchor remember that:
 x =  left(-) and right(+)
 y = straight out at us(+)
 z = up(-) and down(+)
 If you look at the scene file, remember that the top of the node is the part that is facing you first.
*/

/*
    This exttensions holds all the renderer delgate methods.  Inside these methods is where the 3D models
    are placed and rotated based on the given reference images that were found.
 */
extension ViewController: ARSCNViewDelegate, ARSessionDelegate {
    
    // MARK: - Renderer Delegate Methods
    
    // This method gets called by the delegate once one of the reference images is found.
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
        
        // make sure that the anchor is an imageAnchor
        guard let imageAnchor = anchor as? ARImageAnchor else { return }
        
        // grab the name of the image that was detected
        let referenceImageName = imageAnchor.referenceImage.name!
        
        // run this block on a background queue
        queue.async {
            // based on the reference image
            switch referenceImageName {
            case ReferenceImageNames.omWestMiddle.rawValue:
                let referenceNode = self.addNodeToScene(forResource: "OldMain", baseNode: node, foundBuilding: OldMain.shared)
                referenceNode.position = SCNVector3(0.0, -20.0, 0.0)
            case ReferenceImageNames.omQuad.rawValue:
                let referenceNode = self.addNodeToScene(forResource: "OldMain", baseNode: node, foundBuilding: OldMain.shared)
                referenceNode.position = SCNVector3(10.0, -14.0, 5.0)
                referenceNode.eulerAngles.z = .pi
            case ReferenceImageNames.scDoor.rawValue:
                
                // -y = away from us
                // +z = down
                let referenceNode = self.addNodeToScene(forResource: "WestHall", baseNode: node, foundBuilding: WestHall.shared)
                referenceNode.position = SCNVector3(0.0, -25.0, 6.0)
            case ReferenceImageNames.scMiddle.rawValue:
                let referenceNode = self.addNodeToScene(forResource: "WestHall", baseNode: node, foundBuilding: WestHall.shared)
                referenceNode.position = SCNVector3(-10.0, -20.0, 5.0)
            case ReferenceImageNames.oomCenter.rawValue:
                let referenceNode = self.addNodeToScene(forResource: "OldOldMain", baseNode: node, foundBuilding: OldOldMain.shared)
                referenceNode.position = SCNVector3(-4.0, -20.0, 5.0)
                referenceNode.eulerAngles.x = -.pi/2
                referenceNode.eulerAngles.z = .pi
            case ReferenceImageNames.mhDoorNorth.rawValue:
                let referenceNode = self.addNodeToScene(forResource: "MemorialHall", baseNode: node, foundBuilding: MemorialHall.shared)
                referenceNode.position = SCNVector3(0.0, -3.0, 3.0)
            case ReferenceImageNames.mhDoorSouth.rawValue:
                let referenceNode = self.addNodeToScene(forResource: "MemorialHall", baseNode: node, foundBuilding: MemorialHall.shared)
                referenceNode.position = SCNVector3(-13.0, -3.0, 3.5)
            default:
                return
            }
            
            // update the tracking
            self.updateTracking()
        }
        // update the statusbar text to show that a model was added
        self.statusBarViewController.displayAddedModel()
    }
    
    // this method adds the referenceNode to the scene and tells the BuildingController that this certain building was found.  It returns the reference node so that the caller will modify the position and rotation of the node.
    private func addNodeToScene(forResource resource: String, baseNode: SCNNode, foundBuilding: Building) -> SCNReferenceNode {
        // create a reference node to the building in the scene file
        let sceneURL = Bundle.main.url(forResource: resource, withExtension: "scn", subdirectory: "art.scnassets")!
        let referenceNode = SCNReferenceNode(url: sceneURL)!
        referenceNode.load()
        baseNode.addChildNode(referenceNode)
        BuildingController.shared.foundBuildings.insert(foundBuilding)
        return referenceNode
    }
    
    // MARK: - Session Delegate Methods
    
    // this method is used to display a models historical images based on whether the center of the screen is pointing at it.
    func session(_ session: ARSession, didUpdate frame: ARFrame) {
        guard BuildingController.shared.foundBuildings.count >= 0 else { return }

        let location = CGPoint(x: UIScreen.main.bounds.midX, y: UIScreen.main.bounds.midY)
        
        // preform hitTest
        let hitTestResult = self.sceneView.hitTest(location, options: nil)
        
        if let hitTestNode = hitTestResult.first {
            hitTestBuildingCheck(hitTestNode: hitTestNode)
        } else {
            
        }
        
        guard imagePickerHideTimer == nil else { return }
        imagePickerHideTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: false, block: { [weak self] timer in
            DispatchQueue.main.async {
                self?.imagePickerViewController.hide()
            }
            timer.invalidate()
        })
        
    }
    
}

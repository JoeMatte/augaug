//
//  StatusBarViewController.swift
//  ARImageFinder
//
//  Created by Joe Matteson on 8/19/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import UIKit


/*
    This Class controlles the StatusBar inside the main view of the application.  It encapsulates all of it's functionallity and only allows
    other classes to trigger certain predetermined events i.e. the display functions.  This class for the most part runs in the background.
    It schedules timers that run on a background thread which operates serially. For the most part, you can think of the array of timers as a
    queue that runs the timers in FIFO kind of fashion.  This class is also responsible for calling the restart expericence function back in the
    main view controller class.
 */
class StatusBarViewController: UIViewController {
    
    // MARK: - Outlets
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var roundedVisualEffect: RoundedVisualEffect!
    
    // MARK: - Properties
    
    // variable for telling methods that the statusbar is either hidden or already showing
    var isEnabled: Bool = true
    
    // the handler method that will run when the user taps the reset button
    var restartExperienceHandler: () -> Void = {}
    
    // A timer for hiding the statusBar when specified by one of the messages
    private var messageHideTimer: Timer?
    
    // the queue of timers that tell the the label to change its text
    private var timers: [Timer] = []
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // once the statusBar appears it should hide it instantly
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hide(animated: false)
    }
    
    // MARK: - Actions
    
    // if the restart button was tapped remove all previous timers and display the restart experience text sequence
    @IBAction func resetButtonTapped(_ sender: UIButton) {
        dequeueAll()
        displayRestart()
        restartExperienceHandler()
    }
    
    // MARK: - Message Scheduler
    
    // This function actually writes the text to the label inside the statusbar
    private func showMessage(state: StatusLabelState, autoHide: Bool = false) {
        // invalidate any previous hide timer to keep the statusbar showing
        self.messageHideTimer?.invalidate()
        
        // change the text of the label on the main thread to follow convention
        DispatchQueue.main.async {
            self.infoLabel.text = state.rawValue
        }
        
        // show the statusbar if it is not already showing
        self.show()
        
        // if autohide was set to true, set the hideTimer property to a new timer that will hide the bar after 3 seconds
        if autoHide {
            self.messageHideTimer = Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { [weak self] (timer) in
                self?.hide(animated: true)
            }
        }
    }
    
    // sceduales text to be displayed by creating a timer
    private func scheduleText(state: StatusLabelState, inSeconds seconds: TimeInterval, autoHide: Bool = false) {
        // creating the timer based on the parameters of the method
        let timer = Timer.scheduledTimer(withTimeInterval: seconds, repeats: false) { [weak self] timer in
            self?.showMessage(state: state, autoHide: autoHide)
        }
        // append the timer to the timers property
        timers.append(timer)
    }
    
    // cancels all the currently scehdualed timers and removes them from the array
    public func dequeueAll() {
        timers.forEach {
            $0.invalidate()
        }
        timers.removeAll()
    }
    
    // display a debug message based on the string that is passed in.  THIS METHOD IS FOR DEBUG PURPOSES ONLY.  Add the messages to the state enum if needed.
    func displayDebugMessage(message: String) {
        show()
        infoLabel.text = message
    }
    
    // MARK: - Show/Hide Functions
    
    // hides the status bar view
    public func hide(animated: Bool) {
        guard isEnabled else { return }
        DispatchQueue.main.async {
            // grab the current position
            let currentPosition = self.roundedVisualEffect.center
            self.roundedVisualEffect.orginalPosition = currentPosition
            
            // move the position to off the screen
            let x: Double = Double(currentPosition.x) - (2 * Double(currentPosition.x)) - 20
            let y: Double = Double(currentPosition.y)
            
            let newPosition = CGPoint(x: x, y: y)
            
            if animated {
                UIView.animate(withDuration: 1.0) {
                    // asign this new position to the rounded visual effect
                    self.roundedVisualEffect.center = newPosition
                }
            } else {
                // asign this new position to the rounded visual effect
                self.roundedVisualEffect.center = newPosition
            }
        }
        
        // disable the rounded visual effect
        self.isEnabled = false
    }
    
    // show the rounded visual effect using an animation
    private func show() {
        // if the rounded visual effect disabled continue
        guard !isEnabled else { return }
        
        DispatchQueue.main.async {
            // animate the view to the original position
            UIView.animate(withDuration: 0.5) {
                self.roundedVisualEffect.center = self.roundedVisualEffect.orginalPosition
            }
        }
        
        // enable the rounded visual effect
        isEnabled = true
    }
    
    // MARK: - Predefined Message Sequences
    
    // The inSeconds property is the delay before them label is changed
    
    func displayPointCameraAtBuilding() {
        scheduleText(state: .pointAtBuilding, inSeconds: 3)
        scheduleText(state: .tapOnScreen, inSeconds: 6, autoHide: true)
    }
        
    func displayRestart() {
        scheduleText(state: .restarting, inSeconds: 1)
        scheduleText(state: .restarted, inSeconds: 2)
        scheduleText(state: .pointAtBuilding, inSeconds: 4)
        scheduleText(state: .tapOnScreen, inSeconds: 7, autoHide: true)
    }
    
    func displaySessionFailed() {
        scheduleText(state: .sessionFailed, inSeconds: 2)
        scheduleText(state: .sessionFailed2, inSeconds: 10, autoHide: true)
    }
    
    func displayAddedModel() {
        dequeueAll()
        scheduleText(state: .addedModel, inSeconds: 1)
        scheduleText(state: .clickModel, inSeconds: 4, autoHide: true)
    }
    
    func displayClickImage() {
        scheduleText(state: .clickOnAnImage, inSeconds: 1, autoHide: true)
    }
}


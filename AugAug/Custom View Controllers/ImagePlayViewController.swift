//
//  ImagePlayViewController.swift
//  AROldAugsburg
//
//  Created by Joe Matteson on 9/13/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import UIKit

/*
 This class allows the user to zoom in on the image.  It uses a scrollView with specific zoom scales.
 */
class ImagePlayViewController: UIViewController, UIScrollViewDelegate {
    
    // MARK: - Outlets
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    // MARK: - Properties
    var selectedImage: UIImage?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // if the image on the previous screen was selected then there will always be an image
        if let image = selectedImage {
            imageView.image = image
        }
        
        scrollView.delegate = self
        
        // getting the zoom scale
        let widthScale = view.bounds.size.width / imageView.bounds.width
        let heightScale = view.bounds.size.height / imageView.bounds.height
        let scale = min(widthScale, heightScale)
        scrollView.minimumZoomScale = scale
        scrollView.zoomScale = scale
    }
    
    // returns the view that will want to be zoomed
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}

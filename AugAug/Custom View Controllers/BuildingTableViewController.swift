//
//  BuildingTableViewController.swift
//  AugAug
//
//  Created by Joe Matteson on 11/20/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import UIKit

class BuildingTableViewController: UITableViewController {
    
    var selectedBuilding: Building? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return BuildingController.shared.buildings.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "buildingNameCell", for: indexPath)
        let buildings = BuildingController.shared.buildings
        
        cell.textLabel?.text = buildings[buildings.index(buildings.startIndex, offsetBy: indexPath.row)].name

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedBuildingName = tableView.cellForRow(at: indexPath)?.textLabel?.text!
        BuildingController.shared.buildings.forEach { (Building) in
            if Building.name == selectedBuildingName {
                self.selectedBuilding = Building
            }
        }
        performSegue(withIdentifier: "buildingNamePickerSegue", sender: self)
    }
}

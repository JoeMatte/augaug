//
//  ImagePickerViewController.swift
//  AROldAugsburg
//
//  Created by Joe Matteson on 8/27/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import UIKit

/*
 This class holds all the references for the bottom subView in the main view controller.  Its purpose is to display all the images based on the model that was tapped.  There is a prepare statement with a segue to a description view controller where the user can read about the image.
 */
class ImagePickerViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    // MARK: - Outlets
    
    // This outlet is used for the collectionView that holds all the images to the specific build that is being looked at
    @IBOutlet weak var collectionView: UICollectionView!
    
    // MARK: - Properties
    
    // this point is used to hold the original position of this child view.  This is used for the show/ hide animations to return to this original position
    var originalPosition: CGPoint!
    var hiddenPosition: CGPoint = CGPoint(x: 0, y: UIScreen.main.bounds.maxY)
    
    // this is used to disable the view when it is not shown to the user
    var isEnabled: Bool = false
    
    // this is the custom images that were passed in based on the current model being used
    var currentBuilding: Building? {
        didSet {
            collectionView.reloadData()
        }
    }
    
    // this is the image that was selected and it is used for the prepare statement at the bottom of the screen
    var selectedImage: HistoricalImage?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.dataSource = self
        collectionView.delegate = self
        originalPosition = collectionView.layer.position
        collectionView.layer.position = hiddenPosition
    }
    
    // MARK: - Show/Hide
    
    // show the child view using a animations that moves the view up
    func show() {
        // if it is already enabled then there is not point on showing it again
        guard !isEnabled else { return }
        
        DispatchQueue.main.async {
            // animate the view by returning it to its original position
            UIView.animate(withDuration: 1) {
                self.collectionView.layer.position = self.originalPosition
            }
        }
        
        isEnabled = true
    }
    
    // hide the child view from the user by animating it off the screen
    func hide() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1) {
                self.collectionView.layer.position = self.hiddenPosition
            }
        }
        
        isEnabled = false
    }
    
    // MARK: - UICollectionViewDataSource
    
    // return the number of items in each section to the DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let numberOfHistoricalImages = currentBuilding?.historicalImages.count {
            return numberOfHistoricalImages
        } else {
            return 0
        }
    }
    
    // Adds a new Cell with custom data
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // grab a reusable cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageCollectionViewCell
        guard let historicalImages = currentBuilding?.historicalImages else { return cell }
        let currentHistoricalImage = historicalImages[indexPath.row]
        // set the image in the custom cell view
        cell.setImage(image: currentHistoricalImage.image)
        // set the image year
        cell.setYearLabel(imageYear: currentHistoricalImage.year)
        return cell
    }
    
    // preform this method when one of the cells was tapped
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // if the cell is our custom cell type and the cell has a image, continue.
        if (collectionView.cellForItem(at: indexPath) as? ImageCollectionViewCell) != nil {
            let historicalImage = currentBuilding?.historicalImages[indexPath.row]
            selectedImage = historicalImage
            performSegue(withIdentifier: "ImagePicked", sender: self)
        }
    }
    
    // MARK: - Navigation
    
    // all this is doing is passing the selected image to the next viewController
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier,
                identifier == "ImagePicked" else { return }
        guard let navigationController = segue.destination as? UINavigationController,
            let destination = navigationController.topViewController as? ImageDescriptionViewController else { return }
        if let historicalImage = selectedImage {
            destination.selectedImage = historicalImage
        }
        
    }
    
}

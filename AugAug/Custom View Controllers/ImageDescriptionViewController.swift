//
//  ImagePickedViewController.swift
//  AROldAugsburg
//
//  Created by Joe Matteson on 9/12/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import UIKit

/*
 This class is the controller for the image description view.  This view displays the image, the year, and a description of the image
 */
class ImageDescriptionViewController: UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - Properties
    
    // the historical image that gets passed into this viewcontroller
    var selectedImage: HistoricalImage?
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // if there is a selected image (which there will always be once you get to this point) we should set all the views
        if let historicalImage = selectedImage {
            imageView.image = historicalImage.image
            yearLabel.text = historicalImage.year
            descriptionTextView.text = historicalImage.description
        } else {
            // we should never have gotten to this point so return back to the AR session
        }
        
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        guard let segueID = segue.identifier,
            segueID == "ImageTapped" else { return }
        // Pass the selected object to the new view controller.
        if let destination = segue.destination as? ImagePlayViewController,
            let historicalImage = selectedImage {
            destination.selectedImage = historicalImage.image
        }
    }
    

}

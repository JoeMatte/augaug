//
//  Enums.swift
//  AROldAugsburg
//
//  Created by Joe Matteson on 9/13/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import Foundation

// PUT ALL THE NAMES OF THE IMAGES HERE
enum ReferenceImageNames: String {
    case omWestMiddle = "OM_west_middle"
    case omQuad = "OM_quad"
    case scDoor = "WH_door"
    case scMiddle = "WH_middle"
    case oomCenter = "OOM_center"
    case mhDoorNorth = "MH_door_north"
    case mhDoorSouth = "MH_door_south"
}

enum HistoricalImageNames : String {
    case omh1918 = "OM1918SouthWestFaceGroup"
    case omh1926 = "OM1926SouthWestFace"
    case omh1947 = "OM1947NorthFace"
    case omh1950 = "OM1950LibraryInternal"
    
    case wh1948e = "WH1948EastFaceDemolition"
    case wh1948w = "WH1948WestFaceGroup"
    case wh19xx  = "WH19XXSouthWestFace"
    
    case oomCenter = "OOM1872Center"
    case oomLaboratory = "OOM1872Laboratory"
    case oomNorthEast = "OOM1872NorthEast"
    case oomNorthWest = "OOM1872NorthWest"
    case oomLetter = "OOM1895LetterHead"
    case oomSouthEast = "OOM1947SouthEast"
    
    case mhNorthWest = "MH1965northwest"
    case mhNorthEast = "MH1938northeast"
    case mhEast = "MH1950east"
    case mhSouthWest = "MH1940southwest"
}

// make sure to add the type of image to this enum along with the image above
enum BuildingType {
    case OldMain
    case WestHall
    case OldOldMain
    case MemorialHall
}

// the enum for the status of the AR session
enum StatusLabelState: String {
    case pointAtBuilding = "Point camera at a building or"
    case tapOnScreen = "Tap on the screen to select a building"
    case restarting = "Restarting"
    case restarted = "Restarted"
    case addedModel = "Added building model"
    case sessionInterrupted = "Session was interrupted"
    case sessionResumed = "Session resumed"
    case sessionFailed = "Session failed to start"
    case sessionFailed2 = "Please restart app"
    case clickModel = "Click on the model to view images"
    case clickOnAnImage = "Click on an image to view its details"
}

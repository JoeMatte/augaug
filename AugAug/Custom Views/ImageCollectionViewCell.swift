//
//  ImageCollectionViewCell.swift
//  ARImageFinder
//
//  Created by Joe Matteson on 8/27/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    func setImage(image: UIImage) {
        imageView.image = image
    }
    
    func setYearLabel(imageYear year: String) {
        yearLabel.text = year
    }
}

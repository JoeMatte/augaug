//
//  RoundedVisualEffect.swift
//  ARImageFinder
//
//  Created by Joe Matteson on 8/22/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedVisualEffect: UIView {
    
    // TAG: instance variables
    
    // used for enabling and disabling the view
    var isEnabled: Bool = true
    
    // get the initial position of the view so that we can animate to the same spot later
    var orginalPosition: CGPoint!
    
    // initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    // decoder initializer
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    // sets up the view
    private func setup() {
        // roound the edges
        layer.cornerRadius = 10
        clipsToBounds = true
        self.orginalPosition = layer.position
    }
}

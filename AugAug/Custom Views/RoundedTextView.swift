//
//  RoundedTextView.swift
//  AugAug
//
//  Created by Joe Matteson on 9/30/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import UIKit

class RoundedTextView: UITextView {

    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    func setup() {
        layer.cornerRadius = 5
        clipsToBounds = true
    }
}

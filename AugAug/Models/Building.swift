//
//  Building.swift
//  AROldAugsburg
//
//  Created by Joe Matteson on 9/18/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import Foundation
import UIKit
import ARKit

class Building: Hashable {
    var type: BuildingType
    var name: String
    var referenceImages: Set<ARReferenceImage>?
    var historicalImages: [HistoricalImage]
    
    init(type: BuildingType, name: String, referenceImages: Set<ARReferenceImage>?, historicalImages: [HistoricalImage]) {
        self.type = type
        self.name = name
        self.historicalImages = historicalImages
        self.referenceImages = referenceImages
    }
    
    static func == (lhs: Building, rhs: Building) -> Bool {
        return lhs.type == rhs.type
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(type)
    }
}

class OldMain: Building {
    static let shared = OldMain()
    init() {
        super.init(type: .OldMain,
                   name: "Old Main (1902 - Present)",
                   referenceImages: ARReferenceImage.referenceImages(inGroupNamed: "AR Resources OM", bundle: nil),
                   historicalImages: [
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.omh1918.rawValue)!, description: "Group photograph in front of Old Main, circa 1918.", year: "1918"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.omh1926.rawValue)!, description: "Postcard of Old Main, 1926.", year: "1926"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.omh1947.rawValue)!, description: "Old Main, circa 1947.", year: "1947"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.omh1950.rawValue)!, description: "Old Main Library, interior, 1950.", year: "1950")
        ])
    }
}

class WestHall: Building {
    static let shared = WestHall()    
    init() {
        super.init(type: .WestHall,
                   name: "West Hall (1874 - 1948)",
                   referenceImages: ARReferenceImage.referenceImages(inGroupNamed: "AR Resources WH", bundle: nil),
                   historicalImages: [
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.wh19xx.rawValue)!, description: "West Hall, southwest corner, facing northeast.", year: "1910"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.wh1948e.rawValue)!, description: "West Hall, demolition, east facade facing northwest, 1948.", year: "1948"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.wh1948w.rawValue)!, description: "West Hall, west entrance, facing southeast, 1948.", year: "1948"),
        ])
    }
}

class OldOldMain: Building {
    static let shared = OldOldMain()
    init() {
        super.init(type: .OldOldMain,
                   name: "Old Main (1872 - 1949)",
                   referenceImages: ARReferenceImage.referenceImages(inGroupNamed: "AR Resources OOM", bundle: nil),
                   historicalImages: [
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.oomCenter.rawValue)!, description: "Old Main (1872 - 1948), demolition, west and center wings, facing northwest, 1948", year: "1948"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.oomLetter.rawValue)!, description: "Letterhead featuring Augsburg Seminary Campus Buildings, circa 1895", year: "1895"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.oomNorthEast.rawValue)!, description: "Old Main (1872 - 1948), demolition, facing northeast, 1949", year: "1949"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.oomNorthWest.rawValue)!, description: "Old Main (1872 - 1948), northwest corner, facing southeast, circa 1948", year: "1948"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.oomSouthEast.rawValue)!, description: "Sign announcing the future location of the Sverdrup Library and Science Hall, facing southeast, 1947 - 1948", year: "1947"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.oomLaboratory.rawValue)!, description: "Old Main (1872 - 1948), laboratory", year: "1872")
        ])
    }
}

class MemorialHall: Building {
    static let shared = MemorialHall()
    init() {
        super.init(type: .MemorialHall,
                   name: "Memorial Hall (1939 - Present)",
                   referenceImages: ARReferenceImage.referenceImages(inGroupNamed: "AR Resources MH", bundle: nil),
                   historicalImages: [
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.mhNorthEast.rawValue)!, description: "Sverdrup Oftedal Memorial Hall, south facade of north wing and west facade of east wing, facing northeast, circa 1938", year: "1938"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.mhNorthWest.rawValue)!, description: "Sverdrup Oftedal Memorial Hall, south facade of east wing, facing northwest, circa 1965", year: "1965"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.mhEast.rawValue)!, description: "Sverdrup Oftedal Memorial Hall, south quad entrance, facing east, circa 1950", year: "1950"),
                    HistoricalImage(image: UIImage(named: HistoricalImageNames.mhSouthWest.rawValue)!, description: "Sverdrup Oftedal Memorial Hall, east facade, facing southwest, circa 1940", year: "1940")
        ])
    }
}

struct HistoricalImage {
    var image: UIImage
    var description: String
    var year: String
}

//
//  BuildingsController.swift
//  AROldAugsburg
//
//  Created by Joe Matteson on 9/18/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import Foundation
import ARKit

/*
 This class lets other classes know which reference images are still usable in the session.
 */
class BuildingController {
    
    // MARK: - Properties
    
    // singleton instance
    static let shared = BuildingController()
    
    // the buildings that are availible
    var buildings: Set<Building> = [OldMain.shared, WestHall.shared, OldOldMain.shared, MemorialHall.shared]
    
    // a set of the buildings that were found
    var foundBuildings: Set<Building> = []
    
    init() { }
    
    // MARK: - Functions
    
    // This method only finds and returns the reference images that were not already used.
    func getNeededReferenceImages() -> Set<ARReferenceImage> {
        let buildingsNeeded = buildings.symmetricDifference(foundBuildings)
        var referenceImages: Set<ARReferenceImage> = []
        buildingsNeeded.forEach { (Building) in
            referenceImages = referenceImages.union(Building.referenceImages!)
        }
        return referenceImages
    }
    
    // This method grabs all the reference images from every building
    func getAllReferenceImages() -> Set<ARReferenceImage> {
        var referenceImages: Set<ARReferenceImage> = []
        buildings.forEach { (Building) in
            referenceImages = referenceImages.union(Building.referenceImages!)
        }
        return referenceImages
    }
    
    func reset() {
        self.foundBuildings = []
    }
}

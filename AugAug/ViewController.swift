//
//  ViewController.swift
//  ARImageFinder
//
//  Created by Joe Matteson on 5/30/19.
//  Copyright © 2019 Joe Matteson. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController {

    // MARK: - Outlets
    
    // Mark: sceneView outlet
    @IBOutlet var sceneView: ARSCNView!
    
    // MARK: - Properties
    
    // common ARSession that is being used
    var session: ARSession {
        return sceneView.session
    }
    
    // timer for hiding the imagePicker after 10 seconds
    var imagePickerHideTimer: Timer?
    
    // Create a session configuration
    lazy var configuration = ARWorldTrackingConfiguration()
    
    // background thread for models
    lazy var queue = DispatchQueue.global(qos: .background)

    // MARK: - Child View Controller
    
    // grab the instance of the child view controller lazily
    lazy var statusBarViewController: StatusBarViewController = {
        return self.children.lazy.compactMap({
            // returns only the viewContollers of this type
            $0 as? StatusBarViewController
        }).first!
    }()
    
    // grab the instance of the child view controller lazily
    lazy var imagePickerViewController: ImagePickerViewController = {
        return self.children.lazy.compactMap({
            // returns only the viewContollers of this type
            $0 as? ImagePickerViewController
        }).last!
    }()
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set the restart handler to be the restartTracking method in this class
        statusBarViewController.restartExperienceHandler = { [unowned self] in
            self.resetTracking()
        }
        
        // Set the view's delegate
        sceneView.delegate = self
        session.delegate = self
        
        // Prevent the screen from being dimmed to avoid interuppting the AR experience.
        UIApplication.shared.isIdleTimerDisabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetTracking()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        // Pause the view's session
        session.pause()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        statusBarViewController.displayPointCameraAtBuilding()
    }
    
    // MARK: - Reset Traking Methods
    
    // reset all of the tracking session
    func resetTracking() {
        // hide the image picker
        imagePickerViewController.hide()
        
        // grab all the referenceImages from both buildings
        let referenceImages = BuildingController.shared.getAllReferenceImages()
        configuration.detectionImages =  referenceImages
        
        // There are no buildings found so reset the controller
        BuildingController.shared.reset()
        
        sceneView.scene.rootNode.enumerateChildNodes { (node, stop) in
            node.removeFromParentNode()
        }
        
        // Run the view's session
        session.run(configuration, options: [.resetTracking, .removeExistingAnchors])
    }
    
    func updateTracking() {
        // Only grab the reference images to which ever building was not found
        configuration.detectionImages =  BuildingController.shared.getNeededReferenceImages()
        
        // Run the view's session
        session.run(configuration)
    }
    
    // MARK: - Tap gesture action handler
    
    // This method gets called when the user taps on the screen.  The sender paramerter is the location in 2D space of where the screen was tapped.  This method then does a hit test on if the ray where the user touched the screen intercepts the building geometry object.  if it does, then it will show the corresponding picture collectionView.
    @IBAction func screenTapped(_ sender: UITapGestureRecognizer) {
        let location = sender.location(in: sceneView)
        // preform hitTest
        let hitTestResult = sceneView.hitTest(location, options: nil)
        
        if let hitTestNode = hitTestResult.first {
            hitTestBuildingCheck(hitTestNode: hitTestNode)
        } else {
            statusBarViewController.dequeueAll()
            statusBarViewController.hide(animated: false)
            performSegue(withIdentifier: "selectBuildingFromListSegue", sender: self)
        }
       
    }
    
    func hitTestBuildingCheck(hitTestNode: SCNHitTestResult) {
       let node = hitTestNode.node
       
       imagePickerHideTimer?.invalidate()
       imagePickerHideTimer = nil
       
       // if it hit the building node, show the pictures
       switch node.name {
        case "OldMain":
            imagePickerViewController.currentBuilding = OldMain.shared
        case "WestHall":
            imagePickerViewController.currentBuilding = WestHall.shared
        case "OldOldMain":
             imagePickerViewController.currentBuilding = OldOldMain.shared
        case "MemorialHall":
            imagePickerViewController.currentBuilding = MemorialHall.shared
        default:
            // should never hit this moment
            return
       }
       imagePickerViewController.show()
       statusBarViewController.displayClickImage()
    }
    
    // MARK: - ARSCNView session delegate methods
    
    func session(_ session: ARSession, didFailWithError error: Error) {
       // statusBarViewController.displaySessionFailed()
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        //statusBarViewController.updateInfoLabel(state: .sessionInterrupted)
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        //statusBarViewController.updateInfoLabel(state: .sessionResumed)
        resetTracking()
    }
    
    // MARK: - Navigation
    
    // this method is just used to return to the AR session (this view controller)
    @IBAction func unwindToARSession(_ unwindSegue: UIStoryboardSegue) {
        statusBarViewController.viewWillAppear(true)
        if unwindSegue.identifier == "buildingNamePickerSegue" {
            if let tableViewController = unwindSegue.source as? BuildingTableViewController {
                addNodeToScene(foundBuilding: tableViewController.selectedBuilding!)
                imagePickerViewController.currentBuilding = tableViewController.selectedBuilding
                imagePickerViewController.show()
                statusBarViewController.displayClickImage()
            }
        }
    }
    
    private func addNodeToScene(foundBuilding: Building) {
        // create a reference node to the building in the scene file
        var sceneURL: URL?
        var referenceNode: SCNReferenceNode?
        if foundBuilding.name == OldMain.shared.name {
            sceneURL = Bundle.main.url(forResource: "OldMain", withExtension: "scn", subdirectory: "art.scnassets")!
            referenceNode = SCNReferenceNode(url: sceneURL!)!
            referenceNode!.load()
            referenceNode!.eulerAngles.x = .pi/2
            referenceNode!.position = SCNVector3(-1.0, 0.0, 0.0)
            referenceNode!.scale = SCNVector3(0.05, 0.05, 0.05)
        } else if foundBuilding.name == WestHall.shared.name {
            sceneURL = Bundle.main.url(forResource: "WestHall", withExtension: "scn", subdirectory: "art.scnassets")!
            referenceNode = SCNReferenceNode(url: sceneURL!)!
            referenceNode!.load()
            referenceNode!.eulerAngles.y = .pi/2
            referenceNode!.eulerAngles.x = .pi/2
            referenceNode!.position = SCNVector3(0.0, 0.0, -1.5)
            referenceNode!.scale = SCNVector3(0.05, 0.05, 0.05)
        } else if foundBuilding.name == OldOldMain.shared.name {
            sceneURL = Bundle.main.url(forResource: "OldOldMain", withExtension: "scn", subdirectory: "art.scnassets")!
            referenceNode = SCNReferenceNode(url: sceneURL!)!
            referenceNode!.load()
//            referenceNode!.eulerAngles.y = .pi/2
//            referenceNode!.eulerAngles.x = .pi/2
            referenceNode!.position = SCNVector3(1.0, 0.0, 0.0)
            referenceNode!.scale = SCNVector3(0.05, 0.05, 0.05)
        } else if foundBuilding.name == MemorialHall.shared.name {
            sceneURL = Bundle.main.url(forResource: "MemorialHall", withExtension: "scn", subdirectory: "art.scnassets")!
            referenceNode = SCNReferenceNode(url: sceneURL!)!
            referenceNode!.load()
            referenceNode!.eulerAngles.x = .pi/2
            referenceNode!.position = SCNVector3(0.0, 0.0, 1.0)
            referenceNode!.scale = SCNVector3(0.05, 0.05, 0.05)
        }
        sceneView.scene.rootNode.addChildNode(referenceNode!)
        BuildingController.shared.foundBuildings.insert(foundBuilding)
    }
}
